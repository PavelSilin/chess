package controllers;

import model.board.Board;
import model.board.Coordinates;
import model.exceptions.CheckmateException;
import model.exceptions.WrongBoardStateException;
import model.exceptions.WrongTurnException;
import views.CmdView;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Pattern;

public class CmdController {

    private final static Pattern step = Pattern.compile("\\w\\d-\\w\\d");

    private final static Map<String, Coordinates> positionToCoordinates = new HashMap<>();
    static {
        String[] cols = new String[]{"a", "b", "c", "d", "e", "f", "g", "h"};
        String[] rows = new String[]{"1", "2", "3", "4", "5", "6", "7", "8"};
        for (int col = 0; col < cols.length; col++) {
            for (int row = 0; row < rows.length; row++) {
                positionToCoordinates.put(cols[col] + rows[row], new Coordinates(row, col));
            }
        }
    }

    private final Scanner scanner;
    private final Board board;
    private final CmdView view;


    public CmdController() {
        this.board = new Board();
        this.view = new CmdView(board.getCells());
        scanner = new Scanner(System.in);
        view.paint();
    }

    public void start() {
        CheckmateException gameOverMsg = null;
        while (gameOverMsg == null) {
            String[] turn = getTurn();
            Coordinates source = positionToCoordinates.get(turn[0]);
            Coordinates target = positionToCoordinates.get(turn[1]);
            try {
                board.move(source, target);
                view.paint();
            } catch (WrongBoardStateException | WrongTurnException e) {
                System.err.println(e.getMessage());
            } catch (CheckmateException e) {
                gameOverMsg = e;
            }
        }
        System.err.println(gameOverMsg.getMessage());
    }


    public String[] getTurn() {
        String nextTurn = scanner.next().toLowerCase();
        while (!step.matcher(nextTurn).matches()) {
            System.err.println("Wrong command, try again ... ");
            nextTurn = scanner.next();
        }
        return nextTurn.split("-");
    }
}
