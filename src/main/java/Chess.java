import controllers.CmdController;

public class Chess {

    private static final String CMD = "cmd";

    public static void main(String[] args) {
        String mode = args.length == 0 ? CMD : args[0];
        CmdController controller;

        if (mode.equals(CMD)){
            controller = new CmdController();
        } else {
            throw new IllegalArgumentException("Wrong game mode!");
        }
        controller.start();
    }
}
