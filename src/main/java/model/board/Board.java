package model.board;

import model.exceptions.CheckmateException;
import model.exceptions.WrongBoardStateException;
import model.exceptions.WrongTurnException;
import model.pieces.*;

public class Board {

    private static final int DIMENSION = 8;

    private Cell[][] cells;
    private boolean isWhiteTurn;

    public Board() {
        initialiseBoard();
        isWhiteTurn = true;
    }

    public Coordinates[] move(Coordinates source, Coordinates target) throws WrongBoardStateException, WrongTurnException , CheckmateException {
        Cell sourceCell = getCellFrom(source);
        Cell targetCell = getCellFrom(target);

        Piece sourcePiece = sourceCell.getPiece();
        Piece targetPiece = targetCell.getPiece();

        if(sourcePiece == null) {
            throw new WrongBoardStateException("Source piece is not present!");
        }

        if(sourcePiece.getColour() != isWhiteTurn) {
            throw new WrongBoardStateException("Wrong order of turn!");
        }

        if(targetPiece != null && targetPiece.getColour() == sourcePiece.getColour()) {
            if (isPossibleCastling(sourcePiece, targetPiece)) {
                //make castling and return new coordinates for source and target model.pieces
                return castling(sourcePiece, targetPiece);
            } else {
                throw new WrongBoardStateException("You can't attack piece from your team!");
            }
        }

        for (Coordinates coordinates : sourcePiece.tryMove(targetCell)) {
            if (getCellFrom(coordinates).getPiece() != null) {
                throw new WrongBoardStateException("Path of this piece is not free!");
            }
        }

        checkForMate(targetCell.getPiece());
        movePiece(sourcePiece, target);
        isWhiteTurn = !isWhiteTurn;
        return new Coordinates[]{sourcePiece.getCoordinates()};
    }

    private Coordinates[] castling(Piece source, Piece target) {
        King king = (King) (source.getClass() == King.class ? source : target);
        Rock rock = (Rock) (source.getClass() == Rock.class ? source : target);

        //long castling
        if (king.getCoordinates().col > rock.getCoordinates().col) {
            for (int i = 1; i < 4; i++) {
                if (cells[king.getCoordinates().row][i].getPiece() != null) {
                    throw new WrongTurnException("Castling can't be made");
                }
            }
            movePiece(rock, new Coordinates(king.getCoordinates().row, king.getCoordinates().col - 1));
            movePiece(king, new Coordinates(rock.getCoordinates().row, rock.getCoordinates().col - 1));
        }
        //short castling
        else {
            for (int i = 5; i < 7; i++) {
                if (cells[king.getCoordinates().row][i].getPiece() != null) {
                    throw new WrongTurnException("Castling can't be made");
                }
            }
            movePiece(rock, new Coordinates(king.getCoordinates().row, king.getCoordinates().col + 1));
            movePiece(king, new Coordinates(rock.getCoordinates().row, rock.getCoordinates().col + 1));
        }

        isWhiteTurn = !isWhiteTurn;
        king.discardCastling();
        rock.discardCastling();
        return new Coordinates[]{source.getCoordinates(), target.getCoordinates()};
    }

    private boolean isPossibleCastling(Piece sourcePiece, Piece targetPiece) {
        if (sourcePiece.getClass() == King.class && targetPiece.getClass() == Rock.class
                || sourcePiece.getClass() == Rock.class && targetPiece.getClass() == King.class){
            Castleable castlingPiece = (Castleable) sourcePiece;
            Castleable castlingPiece2 = (Castleable) targetPiece;
            if (castlingPiece.isReadyForCastling() && castlingPiece2.isReadyForCastling()) {
                return true;
            }
        }
        return false;
    }

    private void checkForMate(Piece piece) {
        if (piece != null && piece.getClass().equals(King.class)) {
            String winner = isWhiteTurn ? "White" : "Black";
            throw new CheckmateException("CHECKMATE! Winner is: " + winner);
        }
    }

    private void movePiece(Piece piece, Coordinates target) {
        getCellFrom(piece.getCoordinates()).setPiece(null);
        getCellFrom(target).setPiece(piece);
        piece.setCoordinates(target);
    }

    private Cell getCellFrom(Coordinates coordinates) {
        return cells[coordinates.row][coordinates.col];
    }

    private void initialiseBoard() {
        cells = new Cell[DIMENSION][DIMENSION];
        for (int r = 0; r < cells.length; r++) {
            for (int c = 0; c < cells.length; c++) {
                cells[r][c] = new Cell(new Coordinates(r, c));
            }
        }

        placePieces(true);
        placePieces(false);

    }

    private void placePieces(boolean isWhite) {
        Cell[] backRow = isWhite ? cells[0] : cells[7];
        Cell[] frontRow = isWhite ? cells[1] : cells[6];

        for (Cell cell : frontRow) {
            cell.setPiece(new Pawn(isWhite, cell.getCoordinates()));
        }

        Piece[] backPieces = new Piece[] {
                new Rock(isWhite, null),
                new Knight(isWhite, null),
                new Bishop(isWhite, null),
                new Queen(isWhite, null),
                new King(isWhite, null),
                new Bishop(isWhite, null),
                new Knight(isWhite, null),
                new Rock(isWhite, null)
        };

        for (int i = 0; i < backRow.length; i++) {
            Cell cell = backRow[i];
            Piece piece = backPieces[i];
            cell.setPiece(piece);
            piece.setCoordinates(cell.getCoordinates());
        }
    }

    public Cell[][] getCells() {
        return cells;
    }
}
