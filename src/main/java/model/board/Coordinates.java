package model.board;

public class Coordinates {
    public final int col;
    public final int row;

    public Coordinates(int row, int col) {
        this.row = row;
        this.col = col;
    }

    @Override
    public String toString() {
        return "X: " + col + " Y: " + row;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Coordinates that = (Coordinates) o;

        if (col != that.col) return false;
        return row == that.row;
    }

    @Override
    public int hashCode() {
        int result = col;
        result = 31 * result + row;
        return result;
    }
}
