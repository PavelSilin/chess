package model.pieces;

import model.board.Cell;
import model.board.Coordinates;
import model.exceptions.WrongTurnException;

import java.util.Collections;
import java.util.List;

public class Pawn extends Piece {

    boolean canPass;

    public Pawn(boolean colour, Coordinates coordinates) {
        super(colour, coordinates);
        canPass = true;
    }

    @Override
    public List<Coordinates> tryMove(Cell targetCell) throws WrongTurnException {
        Coordinates target = targetCell.getCoordinates();
        Piece piece = targetCell.getPiece();

        int dc = target.col - coordinates.col;
        int dr = target.row - coordinates.row;

        if (dc == 0 && dr == 0 || Math.abs(dc) + Math.abs(dr) > 2) {
            throw new WrongTurnException("Pawn: Wrong length of a move!");
        }

        //white model.pieces is up of model.board
        if (!this.colour && dr > 0 || this.colour && dr < 0) {
            throw new WrongTurnException("Pawn: Wrong direction turn!");
        }

        else if (Math.abs(dr) == 2 && !canPass) {
            throw new WrongTurnException("Pawn: Pass don't allow for this moment!");
        }

        if (piece != null) {
            if (Math.abs(dc) != 1 || Math.abs(dr) != 1) {
                throw new WrongTurnException("Pawn: You can't attack with this direction!");
            }
        } else {
            if (Math.abs(dr) > 2 || dc != 0) {
                throw new WrongTurnException("Pawn: You can't move with this direction!");
            }
        }
        return move(target);
    }

    @Override
    protected List<Coordinates> move(Coordinates target) {
        if (canPass) canPass = false;
        if (Math.abs(target.row - coordinates.row) == 2) {
            return super.move(target);
        }
        return Collections.emptyList();
    }
}
