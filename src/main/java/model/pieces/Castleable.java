package model.pieces;

public interface Castleable {
    boolean isReadyForCastling();
    void discardCastling();
}
