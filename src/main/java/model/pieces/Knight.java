package model.pieces;

import model.board.Cell;
import model.board.Coordinates;
import model.exceptions.WrongTurnException;

import java.util.Collections;
import java.util.List;

public class Knight extends Piece {

    private final static Coordinates[] shifts = new Coordinates[]{
            new Coordinates(1, 2),
            new Coordinates(2, 1)
    };

    public Knight(boolean colour, Coordinates coordinates) {
        super(colour, coordinates);
    }

    @Override
    public List<Coordinates> tryMove(Cell targetCell) throws WrongTurnException {
        for (Coordinates shift : shifts) {
            Coordinates target = targetCell.getCoordinates();
            if (Math.abs(target.row - this.coordinates.row) == shift.row && Math.abs(target.col - this.coordinates.col) == shift.col) {
                return move(target);
            }
        }
        throw new WrongTurnException("Knight: You can't do this move!");
    }

    @Override
    protected List<Coordinates> move(Coordinates target) {
        return Collections.emptyList();
    }
}
