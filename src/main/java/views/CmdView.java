package views;

import model.board.Cell;
import model.pieces.Piece;

public class CmdView {

    private static final String UPPER_BOUND = "  ---------------------------------\n";
    private static final String FREE_SPOT = "|   ";
    private static final String RIGHT_BOUND = "|\n";
    private static final int VIEW_SIZE = 18 * 36;
    private static final String COL_LABELS = "    A   B   C   D   E   F   G   H  \n";

    private Cell[][] cells;
    private StringBuilder viewBuilder;

    public CmdView(Cell[][] cells) {
        this.cells = cells;
        this.viewBuilder = new StringBuilder(VIEW_SIZE);
    }

    public void paint() {
        viewBuilder.delete(0, VIEW_SIZE);
        viewBuilder.append(COL_LABELS);
        for (int i = 0; i < cells.length; i++) {
            Cell[] line = cells[i];
            viewBuilder.append(UPPER_BOUND);
            viewBuilder.append(i + 1).append(" ");
            for (Cell cell : line) {
                String slot = getLabel(cell);
                viewBuilder.append(slot);
            }
            viewBuilder.append(RIGHT_BOUND);
        }
        viewBuilder.append(UPPER_BOUND);
        System.out.println(viewBuilder);
    }

    private String getLabel(Cell cell) {
        Piece piece = cell.getPiece();
        return piece == null ? FREE_SPOT : "| " + piece.getClass().getSimpleName().substring(0,1) + " ";
    }

}
