package model.pieces;

import model.board.Cell;
import model.board.Coordinates;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class KnightTest {

    @Test
    public void knightCanMakeRightMove() {
        Knight knight = new Knight(true, new Coordinates(0, 0));
        Cell targetCell = new Cell(new Coordinates(2, 1));
        List<Coordinates> coordinates = knight.tryMove(targetCell);
        Assert.assertEquals(0, coordinates.size());

        knight = new Knight(true, new Coordinates(8, 8));
        targetCell = new Cell(new Coordinates(6, 7));
        coordinates = knight.tryMove(targetCell);
        Assert.assertEquals(0, coordinates.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void queenTrowIAEIfMoveIsWrong() {
        Rock rock = new Rock(true, new Coordinates(0, 0));
        Cell targetCell = new Cell(new Coordinates(1, 2));
        rock.tryMove(targetCell);
    }

}
