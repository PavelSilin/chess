package model.pieces;

import model.board.Cell;
import model.board.Coordinates;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class BishopTest {

    @Test
    public void bishopCanMakeRightMoveUp() {

        Coordinates[] expectedCoordinates = new Coordinates[]{
                new Coordinates(1, 1),
                new Coordinates(2, 2),
                new Coordinates(3, 3)
        };

        Bishop bishop = new Bishop(true, new Coordinates(0, 0));
        Cell targetCell = new Cell(new Coordinates(4, 4));
        List<Coordinates> coordinates = bishop.tryMove(targetCell);
        Assert.assertEquals(3, coordinates.size());

        Coordinates current;
        Coordinates expected;
        for (int i = 0; i < coordinates.size(); i++) {
            current = coordinates.get(i);
            expected = expectedCoordinates[i];
            Assert.assertEquals(current.row, expected.row);
            Assert.assertEquals(current.col, expected.col);
        }
    }

    @Test
    public void bishopCanMakeRightMoveDown() {

        Coordinates[] expectedCoordinates = new Coordinates[]{
                new Coordinates(7, 1),
                new Coordinates(6, 2),
                new Coordinates(5, 3)
        };

        Bishop bishop = new Bishop(true, new Coordinates(8, 0));
        Cell targetCell = new Cell(new Coordinates(4, 4));
        List<Coordinates> coordinates = bishop.tryMove(targetCell);
        Assert.assertEquals(3, coordinates.size());

        Coordinates current;
        Coordinates expected;
        for (int i = 0; i < coordinates.size(); i++) {
            current = coordinates.get(i);
            expected = expectedCoordinates[i];
            Assert.assertEquals(expected.row, current.row);
            Assert.assertEquals(expected.col, current.col);
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void bishopTrowIAEIfMoveIsWrong() {
        Rock rock = new Rock(true, new Coordinates(0, 0));
        Cell targetCell = new Cell(new Coordinates(1, 2));
        rock.tryMove(targetCell);
    }

}
