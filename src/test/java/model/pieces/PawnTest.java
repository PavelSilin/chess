package model.pieces;

import model.board.Cell;
import model.board.Coordinates;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class PawnTest {

    @Test
    public void pawnCanMoveUpIfCellIsClean() {
        Pawn pawn = new Pawn(true, new Coordinates(0, 0));
        Cell targetCell = new Cell(new Coordinates(1, 0));
        List<Coordinates> coordinates = pawn.tryMove(targetCell);
        Assert.assertEquals(0, coordinates.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void pawnCantMoveUpIfCellIsNotClean() {
        Pawn pawn1 = new Pawn(true, new Coordinates(0, 0));
        Cell targetCell = new Cell(new Coordinates(1, 0));
        Pawn pawn2 = new Pawn(false, new Coordinates(1, 0));
        targetCell.setPiece(pawn2);
        pawn1.tryMove(targetCell);
    }

    @Test
    public void pawnCanMoveDiagonallyIfCellIsNotClean() {
        Pawn pawn1 = new Pawn(true, new Coordinates(0, 0));
        Cell targetCell = new Cell(new Coordinates(1, 1));
        Pawn pawn2 = new Pawn(false, new Coordinates(1, 1));
        targetCell.setPiece(pawn2);
        List<Coordinates> coordinates = pawn1.tryMove(targetCell);
        Assert.assertEquals(0, coordinates.size());
    }

    @Test
    public void pawnCanMakePassing() {
        Coordinates[] expectedCoordinates = new Coordinates[]{
                new Coordinates(1, 0),
        };

        Pawn pawn = new Pawn(true, new Coordinates(0, 0));
        Cell targetCell = new Cell(new Coordinates(2, 0));
        List<Coordinates> coordinates = pawn.tryMove(targetCell);
        Assert.assertEquals(1, coordinates.size());


        Coordinates current;
        Coordinates expected;
        for (int i = 0; i < coordinates.size(); i++) {
            current = coordinates.get(i);
            expected = expectedCoordinates[i];
            Assert.assertEquals(current.row, expected.row);
            Assert.assertEquals(current.col, expected.col);
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void pawnCantMakePassingTwoTimes() {
        Pawn pawn = new Pawn(true, new Coordinates(0, 0));
        Cell targetCell = new Cell(new Coordinates(2, 0));
        List<Coordinates> coordinates = pawn.tryMove(targetCell);
        Assert.assertEquals(1, coordinates.size());

        pawn.coordinates = targetCell.getCoordinates();

        pawn.tryMove(new Cell(new Coordinates(4, 0)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void pawnCantMoveInWrongDirection() {
        Pawn pawn1 = new Pawn(false, new Coordinates(0, 0));
        Cell targetCell = new Cell(new Coordinates(1, 0));
        pawn1.tryMove(targetCell);
    }

}
